package com.daniel.zomato.model

import com.squareup.moshi.Json

data class UserRating(
    @field:Json(name = "rating_text") val rating_text: String,
    @field:Json(name = "rating_color") val rating_color: String
)