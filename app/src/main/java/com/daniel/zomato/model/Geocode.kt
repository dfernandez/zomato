package com.daniel.zomato.model

import com.squareup.moshi.Json

data class Geocode(@field:Json(name = "nearby_restaurants") val nearby_restaurants: List<NearbyRestaurant>)
