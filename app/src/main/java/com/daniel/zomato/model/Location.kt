package com.daniel.zomato.model

import com.squareup.moshi.Json

data class Location(
    @field:Json(name = "latitude") val latitude: String,
    @field:Json(name = "longitude") val longitude: String,
    @field:Json(name = "address") val address: String
)