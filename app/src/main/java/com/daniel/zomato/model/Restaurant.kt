package com.daniel.zomato.model

import com.squareup.moshi.Json

data class Restaurant(
    @field:Json(name = "id") val id: String,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "thumb") val thumb: String,
    @field:Json(name = "user_rating") val user_rating: UserRating,
    @field:Json(name = "price_range") val price_range: String,
    @field:Json(name = "cuisines") val cuisines: String,
    @field:Json(name = "location") val location: Location


)