package com.daniel.zomato.model

import com.squareup.moshi.Json

data class NearbyRestaurant(@field:Json(name = "restaurant") val restaurant: Restaurant )