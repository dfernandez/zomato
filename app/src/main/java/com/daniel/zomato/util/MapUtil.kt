package com.daniel.zomato.util

import com.daniel.zomato.model.Restaurant
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

object MapUtil {
    fun addRestaurantsMarkers(restaurants: List<Restaurant>, map: GoogleMap){
        for (restaurant in restaurants) {
            map.addMarker(
                MarkerOptions()
                    .title(restaurant.name)
                    .position(getRestaurantLatLng(restaurant))
            ).tag = restaurant
        }
    }

    fun moveCameraToBounds(latLngBounds: LatLngBounds, map: GoogleMap, padding: Int){
        val cu = CameraUpdateFactory.newLatLngBounds(latLngBounds, padding)
        map.animateCamera(cu)
    }

    fun getRestaurantLatLng(restaurant: Restaurant) = LatLng(
        restaurant.location.latitude.toDouble(),
        restaurant.location.longitude.toDouble())

    fun getRestaurantsBounds(restaurants: List<Restaurant>): LatLngBounds {
        val builder = LatLngBounds.Builder()
        for (restaurant in restaurants) {
            builder.include(getRestaurantLatLng(restaurant))
        }
        return builder.build()
    }
}