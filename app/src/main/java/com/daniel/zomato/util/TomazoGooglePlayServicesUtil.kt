package com.daniel.zomato.util

import android.app.Activity
import android.content.Context
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

object TomazoGooglePlayServicesUtil {

    fun handleGooglePlayServiceStatus(context: Context) {
        val googlePlayServiceState = getGooglePlayServicesStatus(context)
        if (googlePlayServiceState != ConnectionResult.SUCCESS) {
            showPlayServicesIssue(context, googlePlayServiceState)
        }
    }

    private fun showPlayServicesIssue(context: Context, playServiceState: Int){
        val dialog = GoogleApiAvailability.getInstance()
            .getErrorDialog(context as Activity, playServiceState, 0) {
                context.finish()
            }
        dialog.show()
    }

    private fun getGooglePlayServicesStatus(context: Context) =
        GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)
}