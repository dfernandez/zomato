package com.daniel.zomato

import android.Manifest
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.daniel.zomato.model.Restaurant
import com.daniel.zomato.network.APIClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.daniel.zomato.util.LocationUtil.isCoarseLocationPermissionGranted
import com.daniel.zomato.util.LocationUtil.requestCoarseLocationPermissionGranted
import com.daniel.zomato.util.MapUtil
import com.daniel.zomato.util.TomazoGooglePlayServicesUtil
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import dagger.android.AndroidInjection
import javax.inject.Inject
import android.app.ProgressDialog

class MapsActivity @Inject constructor() : AppCompatActivity(), OnMapReadyCallback {

    companion object{
        const val MAP_PADDING = 15
    }

    private lateinit var mRestaurants: List<Restaurant>
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    lateinit var myLatLng: LatLng

    lateinit var mProgress: ProgressDialog

    @Inject
    lateinit var apiClient: APIClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_maps)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        setupProgressDialog()
    }

    override fun onResume() {
        super.onResume()
        TomazoGooglePlayServicesUtil.handleGooglePlayServiceStatus(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mProgress.show()
        mMap = googleMap
        if(!isCoarseLocationPermissionGranted(this)){
            requestCoarseLocationPermissionGranted(this)
            return
        }
        setupMap()
    }

    private fun setupMap() { //the current location actually
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    myLatLng = LatLng(it.latitude, it.longitude)

                    apiClient.getNearbyRestaurants(
                        it.latitude.toString(),
                        it.longitude.toString(),
                        { restaurants ->
                            mRestaurants = restaurants
                            showMeInMap()
                            showRestaurantsInMap()
                            moveCameraToBounds()
                            setCustomInfoWindow()
                            mProgress.dismiss()
                        },
                        { error -> toast("The error is: ${error.string()}. Closing app, please try again later")
                            mProgress.dismiss()
                            finish()
                        },
                        { e -> toast("The Exception is: $e. Closing app, please try again later")
                            finish()
                            mProgress.dismiss()}
                    )

                }
            }
            .addOnFailureListener { onFail -> print(onFail) }
    }

    private fun setCustomInfoWindow() {
        val infoWindowAdapter = ZomatoInfoWindowAdapter(this)
        mMap.setInfoWindowAdapter(infoWindowAdapter)
    }

    private fun moveCameraToBounds(){
        var bounds = MapUtil.getRestaurantsBounds(mRestaurants)
        bounds = bounds.including(myLatLng)
        MapUtil.moveCameraToBounds(bounds, mMap, MAP_PADDING)
    }

    private fun showMeInMap(){
        mMap.addMarker(MarkerOptions()
            .position(myLatLng)
            .title("I'm here")
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        )
    }

    private fun showRestaurantsInMap() {
        MapUtil.addRestaurantsMarkers(mRestaurants, mMap)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (permissions.first() == Manifest.permission.ACCESS_COARSE_LOCATION &&
                grantResults.first() == PERMISSION_GRANTED){
            setupMap()
        } else {
            mProgress.dismiss()
            finish()
        }
    }

    private fun toast(text: String) =
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()

    private fun setupProgressDialog(){
        mProgress = ProgressDialog(this)
        mProgress.setTitle("Loading")
        mProgress.setMessage("Wait while loading...")
        mProgress.setCancelable(false)
    }


}
