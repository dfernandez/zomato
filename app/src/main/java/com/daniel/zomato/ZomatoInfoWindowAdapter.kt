package com.daniel.zomato

import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import android.app.Activity
import android.content.Context
import android.graphics.Color
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.daniel.zomato.model.Restaurant
import kotlinx.android.synthetic.main.info_window_layout.view.*
import javax.inject.Inject


class ZomatoInfoWindowAdapter @Inject constructor(private val context: Context): GoogleMap.InfoWindowAdapter {
    override fun getInfoContents(marker: Marker?): View? = null

    override fun getInfoWindow(marker: Marker?): View? {
        val view = (context as Activity).layoutInflater
            .inflate(R.layout.info_window_layout, null)

        if(marker?.tag !is Restaurant){
            return null
        }

        val restaurant = marker.tag as Restaurant

        view.nameTv.text = restaurant.name
        view.cuisinesTv.text = restaurant.cuisines


        view.addressTv.text = restaurant.location.address
        view.userRating.text = restaurant.user_rating.rating_text

        setPriceValue(view, restaurant)
        setRatingColor(view, restaurant)
        setThumb(view, restaurant)

        return view
    }

    private fun setPriceValue(view: View, restaurant: Restaurant){
        view.priceRangeTv.text = when(restaurant.price_range.toInt()){
            1 -> "$"
            2 -> "$$"
            3 -> "$$$"
            4 -> "$$$$"
            else -> "No rating"
        }
    }

    private fun setRatingColor(view: View, restaurant: Restaurant){
        try {
            val color = Color.parseColor("#${restaurant.user_rating.rating_color}")
            view.userRating.setTextColor(color)
        }catch (e: IllegalArgumentException){
            println(e)
        }
    }

    private fun setThumb(view: View, restaurant: Restaurant){
        GlideApp.with(view.context)
            .load(restaurant.thumb)
            .circleCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(view.thumbIv)
    }

}