package com.daniel.zomato.network

import com.daniel.zomato.model.Restaurant
import okhttp3.ResponseBody
import java.lang.Exception

interface APIClient {
    fun getNearbyRestaurants(
        lat: String,
        lon:String,
        onSuccess: (restaurants: List<Restaurant>)-> Unit,
        onFailure: (error: ResponseBody)-> Unit,
        onException: (e: Exception) -> Unit
        )
}