package com.daniel.zomato.network

import com.daniel.zomato.model.Restaurant
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import java.lang.Exception
import javax.inject.Inject

class APIClientRetrofit @Inject constructor(private val retrofitService: RetrofitService): APIClient {
    override fun getNearbyRestaurants(
        lat: String,
        lon: String,
        onSuccess: (restaurants: List<Restaurant>) -> Unit,
        onFailure: (error: ResponseBody) -> Unit,
        onException: (e: Exception) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = retrofitService.getGeoCode(lat, lon)

            withContext(Dispatchers.Main) {
                try {
                    if (response.isSuccessful) {
                        response.body()?.let{
                            onSuccess.invoke(it.nearby_restaurants.map {
                                    nearbyRestaurant -> nearbyRestaurant.restaurant }
                            )
                        }
                    } else {
                        response.errorBody()?.let(onFailure)
                    }
                } catch (e: Exception) {
                    onException.invoke(e)
                }
            }
        }
    }
}