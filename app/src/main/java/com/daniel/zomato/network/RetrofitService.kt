package com.daniel.zomato.network

import com.daniel.zomato.BuildConfig
import com.daniel.zomato.model.Geocode
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RetrofitService {
    @Headers("user-key: ${BuildConfig.ZOMATO_API_KEY}")
    @GET("/api/v2.1/geocode")
    suspend fun getGeoCode(
        @Query("lat") lat: String,
        @Query("lon") lon: String
    ): Response<Geocode>
}