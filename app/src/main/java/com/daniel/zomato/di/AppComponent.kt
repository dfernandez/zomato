package com.daniel.zomato.di

import android.app.Application
import com.daniel.zomato.MapsActivity
import com.daniel.zomato.TomazoApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ActivityModule::class, NetworkModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder
        fun build():AppComponent
    }

    fun inject(tomazoApp: TomazoApp)
    fun inject(mapsActivity: MapsActivity)
}