package com.daniel.zomato.di

import com.daniel.zomato.BuildConfig
import com.daniel.zomato.network.APIClient
import com.daniel.zomato.network.APIClientRetrofit
import com.daniel.zomato.network.RetrofitService
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
abstract class NetworkModule {

    @Singleton
    @Binds
    abstract fun apiClientImpl(apiClient: APIClientRetrofit): APIClient


    @Module
    companion object{

        @Singleton
        @Provides
        @JvmStatic
        fun providesRetrofit(): RetrofitService {


            return Retrofit.Builder()
                .baseUrl(BuildConfig.ZOMATO_URL)
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .client(getHTTPClientWithInterceptor())
                .build().create(RetrofitService::class.java)
        }

        private fun getHTTPClientWithInterceptor(): OkHttpClient{
            val logging = HttpLoggingInterceptor()
            logging.apply { logging.level = HttpLoggingInterceptor.Level.BODY  }
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
            return httpClient.build()
        }
    }



}